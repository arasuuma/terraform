variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "endpoint" {
    type = list
}

variable "display_name" {
    type = string
}