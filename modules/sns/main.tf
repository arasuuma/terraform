######SNS CloudWatch Dashboard###########################

data "aws_caller_identity" "default" {}

resource "aws_sns_topic" "cloudwatch-dashboard-sns-topic" {

  name         = "${var.app_name}-${var.environment}-CloudWatch-Dashboard"
  display_name =  var.display_name #"PROD_ENV"

}

resource "aws_sns_topic_subscription" "cloudwatch-dashboard-sns-topic-subscription" {

  topic_arn = aws_sns_topic.cloudwatch-dashboard-sns-topic.arn
  protocol  = "email"
  endpoint  = [var.endpoint]

}

resource "aws_sns_topic_policy" "cloudwatch-dashboard-sns-topic-policy" {

  arn    = aws_sns_topic.cloudwatch-dashboard-sns-topic.arn
  policy = data.aws_iam_policy_document.sns_topic_policy.json

}

data "aws_iam_policy_document" "sns_topic_policy" {
  policy_id = "__default_policy_ID"

  statement {

    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:AddPermission",
    ]

    effect    = "Allow"
    resources = ["${aws_sns_topic.cloudwatch-dashboard-sns-topic.arn}"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        data.aws_caller_identity.default.account_id,
      ]
    }
  }

  statement {
    sid       = "Allow CloudwatchEvents"
    actions   = ["sns:Publish"]
    resources = ["${aws_sns_topic.cloudwatch-dashboard-sns-topic.arn}"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}


####SNS Default_CloudWatch_Alarms_Topic####################
resource "aws_sns_topic" "cloudwatch-alarm-sns-topic" {

  name         = "${var.app_name}-${var.environment}-Default-CloudWatch-Alarms-Topic"
  display_name =  var.display_name

}

resource "aws_sns_topic_subscription" "cloudwatch-alarm-sns-topic-subscription" {

  topic_arn = aws_sns_topic.cloudwatch-alarm-sns-topic.arn
  protocol  = "email"
  endpoint  = [var.endpoint]

}

data "aws_iam_policy_document" "cloudwatch_alarm_sns_topic_policy" {
  policy_id = "__default_policy_ID"

  statement {

    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:AddPermission",
    ]

    effect    = "Allow"
    resources = ["${aws_sns_topic.cloudwatch-alarm-sns-topic.arn}"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        data.aws_caller_identity.default.account_id,
      ]
    }
  }

  statement {
    sid       = "Allow CloudwatchEvents"
    actions   = ["sns:Publish"]
    resources = ["${aws_sns_topic.cloudwatch-alarm-sns-topic.arn}"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}


resource "aws_sns_topic_policy" "cloudwatch-alarm-sns-topic-policy" {

  arn    = aws_sns_topic.cloudwatch-alarm-sns-topic.arn
  policy = data.aws_iam_policy_document.cloudwatch_alarm_sns_topic_policy.json

}