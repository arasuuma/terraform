/*
Input variables used to configure the 'ecr' module
*/

variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "repositories" {
  type = list
}
