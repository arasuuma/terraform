/*
This module creates a list of ECR repositories as passed to it in a list variable
It also makes sure all images older than 45 days in the repository are deleted
*/

# Create the Docker container repositories
resource "aws_ecr_repository" "repository" {
  count = length(var.repositories)
  name  = var.repositories[count.index]

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

# Add a lifecycle policy to the repositories
resource "aws_ecr_lifecycle_policy" "repository_lifecycle_policy" {
  count       = length(var.repositories)
  repository  = var.repositories[count.index]

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "remove-old-images",
            "selection": {
                "tagStatus": "any",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 45
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}
