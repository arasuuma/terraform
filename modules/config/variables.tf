/*
Input variables used to configure the 'config' module
*/

variable "app_name" {

  type = string

}

variable "environment" {

  type = string

}

variable "include_global_resource_types" {

  description = "Specifies whether AWS Config includes all supported types of global resources with the resources that it records."
  type        = bool
  default     = true

}

variable "config_delivery_frequency" {

  description = "The frequency with which AWS Config delivers configuration snapshots."
  type        = string

}

variable "config_logs_prefix" {

  description = "The S3 prefix for AWS Config logs."
  type        = string
  
}