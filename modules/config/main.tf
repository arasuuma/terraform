####AWS Config Role###########################
resource "aws_iam_role" "config" {
  name = "${var.app_name}-${var.environment}-AWSServiceRoleForConfig"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "config.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "ConfigPolicyAttachement" {

  role       = aws_iam_role.config.id
  policy_arn = "arn:aws:iam::aws:policy/aws-service-role/AWSConfigServiceRolePolicy"

}

###AWS Config Service###########################

data "aws_caller_identity" "default" {}

resource "aws_config_aggregate_authorization" "config_default" {
  
  account_id = [data.aws_caller_identity.default.account_id]
  region     = [data.aws_caller_identity.default.region]
}

resource "aws_config_configuration_recorder_status" "main" {
  
  name       = aws_config_configuration_recorder.main.name
  is_enabled = true
  depends_on = [aws_config_delivery_channel.main]
}

resource "aws_config_delivery_channel" "main" {
  
  name           = "${var.app_name}-${var.environment}-aws-config"
  s3_bucket_name = aws_s3_bucket.s3_config.bucket
  s3_key_prefix  = var.config_logs_prefix 
  #sns_topic_arn  = var.config_sns_topic_arn

  snapshot_delivery_properties {
    delivery_frequency = var.config_delivery_frequency 
  }

  depends_on = [aws_config_configuration_recorder.main]
}

resource "aws_config_configuration_recorder" "main" {
  
  name     = "${var.app_name}-${var.environment}-aws-config"
  role_arn = aws_iam_role.config.arn

  recording_group {
    all_supported                 = true
    include_global_resource_types = var.include_global_resource_types
  }
}

#####S3Rule#######################
resource "aws_config_config_rule" "s3_access_check" {

  name = "s3-account-level-public-access-blocks"
  description = "Checks whether the required public access block settings are configured from account level. The rule is NON_COMPLIANT when the public access block settings are not configured from account level."

  source {
    owner             = "AWS"
    source_identifier = "s3-account-level-public-access-blocks"
  }

  depends_on = [aws_config_configuration_recorder.main]
}

resource "aws_config_config_rule" "s3_server_side" {

  name = "s3-bucket-server-side-encryption-enabled"
  description = "Checks that your Amazon S3 bucket either has S3 default encryption enabled or that the S3 bucket policy explicitly denies put-object requests without server side encryption."

  source {
    owner             = "AWS"
    source_identifier = "s3-bucket-server-side-encryption-enabled"
  }

  depends_on = [aws_config_configuration_recorder.main]
}

resource "aws_config_config_rule" "access_keys_rotated" {

  name = "access-keys-rotated"
  description = "Checks whether the active access keys are rotated within the number of days specified in maxAccessKeyAge. The rule is non-compliant if the access keys have not been rotated for more than maxAccessKeyAge number of days."

  source {
    owner             = "AWS"
    source_identifier = "access-keys-rotated"
  }

  depends_on = [aws_config_configuration_recorder.main]
}



######Create a S3 bucket for the Config logs################

resource "aws_s3_bucket" "s3_config" {

  bucket = "config-bucket-${data.aws_caller_identity.default.account_id}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSConfigBucketPermissionsCheck",
            "Effect": "Allow",
            "Principal": {
                "Service": "config.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::config-bucket-${data.aws_caller_identity.default.account_id}"
        },
        {
            "Sid": "AWSConfigBucketDelivery",
            "Effect": "Allow",
            "Principal": {
                "Service": "config.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::config-bucket-${data.aws_caller_identity.default.account_id}/AWSLogs/${data.aws_caller_identity.default.account_id}/Config/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
EOF

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}