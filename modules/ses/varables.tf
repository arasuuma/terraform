variable "main_dns_zone_id" {
    type = string
}

variable "domain_name" {
    type = string
}

variable "email" {
    type = list
}

variable "verify_domain" {
  type        = bool
  description = "If provided the module will create Route53 DNS records used for domain verification."
  
}

variable "verify_dkim" {
  type        = bool
  description = "If provided the module will create Route53 DNS records used for DKIM verification."
  
}
