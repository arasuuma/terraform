/*
Create SES domain identity and verify it with Route53 DNS records
*/

resource "aws_ses_domain_identity" "ses_domain" {
  
  domain = var.domain_name

}

resource "aws_route53_record" "amazonses_verification_record" {
  
  zone_id = var.main_dns_zone_id
  name    = "_amazonses.${var.domain_name}"
  verify_domain = var.verify_domain
  type    = "TXT"
  ttl     = "600"
  records = [join("", aws_ses_domain_identity.ses_domain.*.verification_token)]

}

resource "aws_ses_domain_identity_verification" "domain_identity_verification" {

  domain = aws_ses_domain_identity.ses_domain.id

  depends_on = [aws_route53_record.amazonses_verification_record]

}


resource "aws_ses_domain_dkim" "ses_domain_dkim" {
  
  domain = join("", aws_ses_domain_identity.ses_domain.domain)

}

resource "aws_route53_record" "amazonses_dkim_record" {
  
  count   =  3
  verify_dkim = var.verify_dkim
  zone_id = var.main_dns_zone_id
  name    = "${element(aws_ses_domain_dkim.ses_domain_dkim.dkim_tokens, count.index)}._domainkey.${var.domain_name}"
  type    = "CNAME"
  ttl     = "600"
  records = ["${element(aws_ses_domain_dkim.ses_domain_dkim.dkim_tokens, count.index)}.dkim.amazonses.com"]

}


##########SES MAIL FROM Domain##################################
/*
resource "aws_ses_domain_mail_from" "main" {

  domain           = aws_ses_domain_identity.ses_domain.domain
  #mail_from_domain = var.mail_from_domain #"eu-central-1.amazonses.com"

}

############SPF validation record#####################

resource "aws_route53_record" "spf_mail_from" {
  
  zone_id = var.main_dns_zone_id
  name    = aws_ses_domain_mail_from.main.mail_from_domain
  type    = "TXT"
  ttl     = "600"
  records = ["v=spf1 include:amazonses.com -all"]
}

resource "aws_route53_record" "spf_domain" {
  
  zone_id = var.main_dns_zone_id
  name    = var.domain_name
  type    = "TXT"
  ttl     = "600"
  records = ["v=spf1 include:amazonses.com -all"]
}

#######Sending MX Record#####################

data "aws_region" "current" {

}

resource "aws_route53_record" "mx_send_mail_from" {

  zone_id = var.main_dns_zone_id
  name    = aws_ses_domain_mail_from.main.mail_from_domain
  type    = "MX"
  ttl     = "600"
  records = ["10 feedback-smtp.${data.aws_region.current.name}.amazonses.com"]

}
*/
#######Receiving MX Record#####################
data "aws_region" "current" {
}

resource "aws_route53_record" "mx_receive" {
  zone_id = var.main_dns_zone_id
  name    = var.domain_name
  type    = "MX"
  ttl     = "1800"
  records = ["10 inbound-smtp.${data.aws_region.current.name}.amazonaws.com"]
}
############email address to assign to SES########################
resource "aws_ses_email_identity" "email" {

  email = var.email #"email@example.com"
  
}