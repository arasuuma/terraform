/*
This module deploys the ECS task definitions and ECS service for running the
PG Alpha Api application
*/


# Create the Cloudwatch log groups required by the API app
resource "aws_cloudwatch_log_group" "ecs_service_log_group" {
  name = "/ecs/${var.app_name}-${var.environment}-application"
}

# A target group for the API ECS service
resource "aws_lb_target_group" "ecs_service_tg" {
  name                 = "${var.app_name}-${var.environment}-tg"
  port                 = var.listen_port
  protocol             = "HTTP"
  vpc_id               = var.vpc_id
  target_type          = var.target_type
  deregistration_delay = "30"

  health_check {
    interval            = var.health_check_interval
    path                = var.health_check_path
    port                = var.listen_port
    protocol            = "HTTP"
    timeout             = var.health_check_timeout
    healthy_threshold   = "2"
    unhealthy_threshold = "3"
    matcher             = var.health_check_matcher
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

/*
Set up the task definitions with the container definitions found in the
container-definitions directory
*/
data "template_file" "container_definition_template" {
  template = "${file("${path.module}/container-definitions/api.json.tpl")}"

  vars = {
    listen_port     = var.listen_port
    cpu             = var.cpu
    memory          = var.memory
    app_name        = var.app_name
    environment     = var.environment
    container_image = var.container_image
  }
}

resource "aws_ecs_task_definition" "ecs_task_definition" {
  family                    = "${var.app_name}-${var.environment}-api"
  container_definitions     = data.template_file.container_definition_template.rendered
  network_mode              = "awsvpc"
  cpu                       = var.cpu
  memory                    = var.memory
  requires_compatibilities  = ["FARGATE", "EC2"]
  task_role_arn             = aws_iam_role.ecs_app_task_role.arn
  execution_role_arn        = aws_iam_role.ecs_app_task_execution_role.arn
  depends_on                = [aws_cloudwatch_log_group.ecs_service_log_group, aws_iam_role.ecs_app_task_role, aws_iam_role.ecs_app_task_execution_role]

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}


# Define the ECS service that will run the application task definiton
resource "aws_ecs_service" "ecs_service" {
  name                                = "${var.app_name}-${var.environment}-api"
  cluster                             = var.ecs_cluster_id
  task_definition                     = aws_ecs_task_definition.ecs_task_definition.arn
  desired_count                       = var.desired_task_count
  deployment_maximum_percent          = var.deployment_maximum_percent
  deployment_minimum_healthy_percent  = var.deployment_minimum_healthy_percent
  health_check_grace_period_seconds   = var.health_check_grace_period_seconds
  launch_type                         = "FARGATE"
  platform_version                    = var.platform_version
  depends_on                          = [aws_ecs_task_definition.ecs_task_definition]

  load_balancer {
    target_group_arn  = aws_lb_target_group.ecs_service_tg.arn
    container_name    = "${var.app_name}-${var.environment}-api"
    container_port    = var.listen_port
  }

  network_configuration {
    subnets             = var.private_subnet_ids
    security_groups     = [aws_security_group.ecs_service.id]
    assign_public_ip    = var.assign_public_ip
  }
}
