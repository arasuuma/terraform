/*
Security groups for the 'ecs-service' module
*/

# Define a security group for the ECS Service
resource "aws_security_group" "ecs_service" {
  name        = "${var.app_name}_${var.environment}_ecs_service_sg"
  description = "Ports needed by the Api ECS Service"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = var.listen_port
    to_port     = var.listen_port
    protocol    = "TCP"
    cidr_blocks = var.allowed_cidrs
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}
