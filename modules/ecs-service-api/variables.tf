/*
Input variables used to configure the 'ecs-service' module
*/

variable "vpc_id" {
  type = string
}

variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "container_image" {
  type = string
}

variable "cpu" {
  type = number
}

variable "memory" {
  type = number
}

variable "assign_public_ip" {
  type = bool
}

variable "platform_version" {
  type    = string
  default = "LATEST"
}

variable "deployment_maximum_percent" {
  type    = number
  default = 200
}

variable "deployment_minimum_healthy_percent" {
  type    = number
  default = 100
}

variable "health_check_grace_period_seconds" {
  type    = number
  default = 15
}

variable "private_subnet_ids" {
  type = list
}

variable "listen_port" {
  type        = number
  description = "The port on which the application will be exposed to the network"
}

variable "target_type" {
  type    = string
  default = "instance"
}

variable "health_check_path" {
  type = string
}

variable "health_check_timeout" {
  type    = number
  default = 5
}

variable "health_check_interval" {
  type    = number
  default = 30
}

variable "health_check_matcher" {
  type    = string
  default = "200,301,302"
}

variable "allowed_cidrs" {
  type = list
}

variable "desired_task_count" {
  type        = number
  description = "The number of tasks that the service should run"
}

variable "ecs_cluster_id" {
  type        = string
  description = "The ECS cluster id that the application should run on"
}

variable "ecs_services_iam_role" {
  type        = string
  description = "IAM Role used by the ECS Service"
}
