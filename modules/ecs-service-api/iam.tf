/*
Create the IAM roles and policies required by ECS to run the Api app
*/

resource "aws_iam_role" "ecs_app_task_role" {
  name = "ecsAlphaApiTaskRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "ecs_app_task_role_policy" {
  name        = "ecsAlphaApiTaskPolicy"
  description = "A policy for the ECS task running the PG Alpha Api"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*",
        "ecr:*",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "ssm:*",
        "secretsmanager:GetSecretValue",
        "kms:Decrypt"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_app_task_role_attach_1" {
  role       = aws_iam_role.ecs_app_task_role.name
  policy_arn = aws_iam_policy.ecs_app_task_role_policy.arn
}

resource "aws_iam_role" "ecs_app_task_execution_role" {
  name = "ecsAlphaApiTaskExecutionRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_app_task_execution_role_attach_1" {
  role       = aws_iam_role.ecs_app_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}
