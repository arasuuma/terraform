/*
Outputs from the 'ecs-service' module
*/

output "ecs_service_tg" {
  value = aws_lb_target_group.ecs_service_tg.arn
}

output "ecs_service_sg_arn" {
  value = aws_security_group.ecs_service.arn
}

output "executionRoleArn" {
  value = aws_iam_role.ecs_app_task_execution_role.arn
}

output "api_service_name" {
  value = aws_ecs_service.ecs_service.name
}

