/*
Outputs from the 'route53' module
*/

output "certificate_arn" {
  value = aws_acm_certificate.certificate.arn
}
