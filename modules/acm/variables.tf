/*
Input variables used to configure the 'acm' module
*/

variable "domain_name" {
  type = string
}

variable "alternative_names" {
  type = list
}

variable "dns_zone_id" {
  type = string
}

variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}
