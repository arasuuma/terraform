/*
Outputs from the 'route53' module
*/

output "main_dns_zone_id" {
  value = aws_route53_zone.main.zone_id
}

output "main_name_servers" {
  value = aws_route53_zone.main.name_servers
}
