/*
Simple module that creates a Route 53 DNS zone file
*/

resource "aws_route53_zone" "main" {
  name = var.domain_name

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}
