/*
Input variables used to configure the 'route53' module
*/

variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "domain_name" {
  type = string
}
