/*
This module defines an Application Load Balancer and attaches a Target Group
to it. This Target Group will be used by other modules to attach resources to,
like ECS services or EC2 instances
*/

# Create a S3 bucket for the ALB access logs
resource "aws_s3_bucket" "alb_access_logs" {
  bucket = "${var.app_name}-${var.environment}-alb-access-logs"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "054676820928"
        ]
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${var.app_name}-${var.environment}-alb-access-logs/*"
    }
  ]
}
EOF

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

# Define an internet-facing application load balancer
resource "aws_lb" "application_lb" {
  name                             = "${var.app_name}-${var.environment}-lb"
  internal                         = var.internal
  load_balancer_type               = "application"
  security_groups                  = [aws_security_group.alb_sg.id]
  subnets                          = var.public_subnet_ids
  enable_deletion_protection       = false
  enable_cross_zone_load_balancing = var.enable_cross_zone_load_balancing
  enable_http2                     = var.enable_http2
  idle_timeout                     = var.idle_timeout
  ip_address_type                  = "ipv4"

  access_logs {
    bucket  = aws_s3_bucket.alb_access_logs.bucket
    prefix  = "${var.app_name}-${var.environment}-alb"
    enabled = true
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

# HTTP listener
resource "aws_lb_listener" "application_http_listener" {
  load_balancer_arn = aws_lb.application_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

# HTTPS listener
resource "aws_lb_listener" "application_https_listener" {
  load_balancer_arn = aws_lb.application_lb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = var.ssl_policy
  certificate_arn   = var.ssl_certificate_arn

  default_action {
    target_group_arn = var.frontend_tg_arn
    type             = "forward"
  }
}

# A rule for the HTTPS Listener to forward API traffic to the API tg
resource "aws_lb_listener_rule" "application_https_listener_rule_api" {
  listener_arn = aws_lb_listener.application_https_listener.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = var.api_tg_arn
  }
}

/*
# Add the DNS names for public access to the application
resource "aws_route53_record" "app_public_record" {
  zone_id = var.dns_zone_id
  name    = var.domain_name
  type    = "A"

  alias {
    name                   = aws_lb.application_lb.dns_name
    zone_id                = aws_lb.application_lb.zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "app_public_record_1" {
  zone_id = var.dns_zone_id
  name    = "api.${var.domain_name}"
  type    = "CNAME"
  ttl     = "300"
  records = [var.domain_name]
}

*/