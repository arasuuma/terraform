/*
Outputs from the 'alb' module
*/

output "application-lb-name" {
  value = aws_lb.application_lb.dns_name
}

output "application-lb-zone-id" {
  value = aws_lb.application_lb.zone_id
}

output "alb_arn" {
  value = aws_lb.application_lb.arn_suffix
}

output "alb_name" {
  value = aws_lb.application_lb.name
}