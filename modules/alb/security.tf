/*
Security groups for the 'alb' module
*/

resource "aws_security_group" "alb_sg" {
  name        = "${var.app_name}-${var.environment}-alb-sg"
  description = "Ports needed by the Application Load Balancer"
  vpc_id      = var.vpc_id

  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = var.allowed_cidrs
  }
  ingress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = var.allowed_cidrs
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}
