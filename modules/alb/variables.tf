/*
Input variables used to configure the 'alb' module
*/

variable "vpc_id" {
  type = string
}

variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "enable_cross_zone_load_balancing" {
  type    = bool
  default = true
}

variable "enable_http2" {
  type    = bool
  default = true
}

variable "idle_timeout" {
  type    = number
  default = 30
}

variable "ssl_certificate_arn" {
  type = string
}

variable "ssl_policy" {
  type = string
}

variable "public_subnet_ids" {
  type = list
}

variable "allowed_cidrs" {
  type = list
}

variable "internal" {
  type    = bool
  default = false
}


variable "api_tg_arn" {
  type = string
}
/*
variable "dns_zone_id" {
  type = string
}

variable "domain_name" {
  type = string
}

*/