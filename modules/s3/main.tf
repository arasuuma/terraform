/*
This module creates the required S3 buckets for the  TF application logs

resource "aws_s3_bucket" "terraform_state_bucket_logs" {
  bucket = "${var.app_name}-${var.environment}-terraform-state-logs"
  acl    = "log-delivery-write"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "${var.app_name}-${var.environment}-terraform-state"
  acl    = "private"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = aws_s3_bucket.user_profiles_bucket_logs.id
    target_prefix = "log/"
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

resource "aws_s3_bucket" "user_profiles_bucket_logs" {
  bucket = "${var.app_name}-${var.environment}-logs"
  acl    = "log-delivery-write"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

resource "aws_s3_bucket" "user_profiles" {
  bucket = "${var.app_name}-${var.environment}"
  acl    = "public-read"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = aws_s3_bucket.user_profiles_bucket_logs.id
    target_prefix = "log/"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

resource "aws_s3_bucket_policy" "user_profiles_bucket_policy" {
  bucket = aws_s3_bucket.user_profiles.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "bucket_policy_user_profiles",
  "Statement": [
    {
      "Sid": "PublicRead",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${var.app_name}-${var.environment}/*"
    }
  ]
}
POLICY
}
