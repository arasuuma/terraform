/*
Input variables used to configure the 's3' module
*/

variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}
