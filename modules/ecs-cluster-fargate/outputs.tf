/*
Outputs from the 'ecs-cluster-fargate' module
*/

output "cluster_id" {
  value = aws_ecs_cluster.ecs_cluster.id
}

output "cluster_name" {
  value = aws_ecs_cluster.ecs_cluster.name
}

output "ecs-service-iam-role-arn" {
  value = aws_iam_role.ecs_fargate_service_role.arn
}
