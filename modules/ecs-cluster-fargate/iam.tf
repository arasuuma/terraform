/*
Here we create all the IAM related resources required by the ECS Fargate cluster
*/

# IAM role for ECS services
resource "aws_iam_role" "ecs_fargate_service_role" {
  name = "${var.app_name}_${var.environment}_ecsServiceRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["ecs.amazonaws.com", "ec2.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs-fargate-service-role-attach-policy-1" {
  role       = aws_iam_role.ecs_fargate_service_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole"
}
