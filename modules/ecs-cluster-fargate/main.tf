/*
This module creates an ECS Fargate cluster where we can run the applications
*/

# Set up the ECS cluster
resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.app_name}-${var.environment}"

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}
