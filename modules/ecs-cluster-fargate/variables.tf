/*
Input variables used to configure the 'ecs-cluster-fargate' module
*/

variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}
