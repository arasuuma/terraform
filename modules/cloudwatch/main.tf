#######ECS API CPU Utilization##########################
resource "aws_cloudwatch_metric_alarm" "ecs_api_cpu" {

  alarm_name          = "ECS-API-CPU-Util"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  service_name        = var.api_service_name
  cluster_name        = var.cluster_name
  period              = "300"
  statistic           = "Average"
  threshold           = "70"
  alarm_description   = "Alert on ECS backend for CPU utilization."
  datapoints_to_alarm = "1"
  treat_missing_data  = "Treat missing data as bad (breaching threshold)"
  alarm_actions       = [var.topic_arn]
  ok_actions          = [var.topic_arn]
  tags = {
    Environment = var.environment
    Application = var.app_name
  }

  dimensions = {
    ClusterName = var.cluster_name
    
  }

}

#######ECS API Memory Utilization##########################
resource "aws_cloudwatch_metric_alarm" "ecs_api_memory" {

  alarm_name          = "ECS-API-Memory-Util"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "MemoryUtilization"
  namespace           = "AWS/ECS"
  service_name        = var.api_service_name
  cluster_name        = var.cluster_name
  period              = "300"
  statistic           = "Average"
  threshold           = "70"
  alarm_description   = "Alert on ECS backend for Memory utilization."
  datapoints_to_alarm = "1"
  treat_missing_data  = "Treat missing data as bad (breaching threshold)"
  alarm_actions       = [var.topic_arn]
  ok_actions          = [var.topic_arn]
  tags = {
    Environment = var.environment
    Application = var.app_name
  }

  dimensions = {
    ClusterName = var.cluster_name
    
  }

}

#######Test2##########################
resource "aws_cloudwatch_metric_alarm" "test" {

  alarm_name          = "Test2"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "Test2token is not valid"
  namespace           = "Test2"
  period              = "300"
  statistic           = "Sum"
  threshold           = "100"
  alarm_description   = "token is not valid."
  datapoints_to_alarm = "1"
  treat_missing_data  = "Treat missing data as missing"
  alarm_actions       = [var.topic_arn]
  ok_actions          = [var.topic_arn]
  tags = {
    Environment = var.environment
    Application = var.app_name
  }

  dimensions = {
    
    
  }

}


#######ALB##########################
resource "aws_cloudwatch_metric_alarm" "lb" {

  alarm_name          = "No Request Received on LB"
  comparison_operator = "LowerThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "RequestCount"
  namespace           = "AWS/ApplicationELB"
  LoadBalancer        = "apps/${var.alb_name}"
  period              = "86400"
  statistic           = "Average"
  threshold           = "0.04"
  alarm_description   = "request count on the LB"
  datapoints_to_alarm = "1"
  treat_missing_data  = "Treat missing data as missing"
  alarm_actions       = [var.topic_arn]
  ok_actions          = [var.topic_arn]
  tags = {
    Environment = var.environment
    Application = var.app_name
  }

  dimensions = {
    LoadBalancer = "apps/${var.alb_name}" #var.alb_name
    
  }

}

#######PgExceptionHandler##########################
resource "aws_cloudwatch_metric_alarm" "PgExceptionHandler" {

  alarm_name          = "ExceptionHandler"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "Testc.p.f.e.handler.PgExceptionHandler"
  namespace           = "Test"
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  alarm_description   = "PgExceptionHandler Error"
  datapoints_to_alarm = "1"
  treat_missing_data  = "Treat missing data as missing"
  alarm_actions       = [var.topic_arn]
  insufficient_data_actions = [var.topic_arn]
  tags = {
    Environment = var.environment
    Application = var.app_name
  }

  dimensions = {
    
    
  }

}