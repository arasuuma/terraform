variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "api_service_name" {
    type = string
}

variable "cluster_name" {
    type = string
}

variable "topic_arn" {
    type = string
}

variable "api_service_name" {
    type = string
}

variable "alb_name" {
    type = string
}

variable "api_tg_arn" {
  type = string
}

