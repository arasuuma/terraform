######Cloudwatch Dashboard###################
resource "aws_cloudwatch_dashboard" "cloudwatch_dashboard" {

  dashboard_name = "Dashboard-${var.app_name}-${var.environment}-ENV"

  dashboard_body = <<EOF
{
    "widgets": [
        {
            "height": 3,
            "width": 6,
            "y": 0,
            "x": 0,
            "type": "metric",
            "properties": {
                "title": "ECS-API-Memory-Util",
                "annotations": {
                    "alarms": [
                        aws_cloudwatch_metric_alarm.ecs_api_memory.arn
                    ]
                },
                "view": "singleValue"
            }
        },
        {
            "height": 3,
            "width": 6,
            "y": 0,
            "x": 6,
            "type": "metric",
            "properties": {
                "title": "ECS-API-CPU-Util",
                "annotations": {
                    "alarms": [
                        aws_cloudwatch_metric_alarm.ecs_api_cpu.arn
                    ]
                },
                "view": "singleValue"
            }
        },
        {
            "height": 6,
            "width": 6,
            "y": 3,
            "x": 18,
            "type": "metric",
            "properties": {
                "view": "timeSeries",
                "stacked": true,
                "metrics": [
                    [ "AWS/ApplicationELB", "HealthyHostCount", "TargetGroup", "LoadBalancer", "app/${var.alb_name}" ],
                    [ "...", "targetgroup/${var.api_tg_arn}", ".", "." ]
                ],
                "region": "eu-central-1",
                "title": "API&UI - HealthyHostCount",
                "setPeriodToTimeRange": true
            }
        }
    ]
}
EOF
}  


######Cloudwatch Dashboard Test-Logs-Dash###################
resource "aws_cloudwatch_dashboard" "cloudwatch_test_logs" {

  dashboard_name = "Dashboard-${var.app_name}-${var.environment}-Test-Logs-Dash"

  dashboard_body = <<EOF
{
    "widgets": [
        {
            "height": 6,
            "width": 24,
            "y": 0,
            "x": 0,
            "type": "log",
            "properties": {
                "query": "SOURCE '/ecs/*' | fields @timestamp, @message\n| filter @message like 'token is not valid'\n| sort @timestamp desc\n| stats count(*) by bin(5m)\n| limit 100",
                "region": "eu-central-1",
                "title": "Logs Dash",
                "view": "timeSeries",
                "stacked": false
            }
        }
    ]
}
EOF
}  

######Cloudwatch Dashboard SNS Event Trigger###################
resource "aws_cloudwatch_event_rule" "rule" {

  name                = "${var.app_name}-${var.environment}-Prod-Restart-Rule"
  description         = "creating event on ECS for Pending State. It will trigger mail when our ECS is restarted."
  
  event_pattern = <<PATTERN
{
  "source": [
    "aws.ecs"
  ],
  "detail-type": [
    "ECS Task State Change"
  ],
  "detail": {
    "lastStatus": [
      "PENDING"
    ]
  }
}  
PATTERN
}


resource "aws_cloudwatch_event_target" "sns" {
  rule      = "${aws_cloudwatch_event_rule.rule.name}"
  target_id = "SendToSNS"
  arn       = var.topic_arn #"${aws_sns_topic.aws_logins.arn}"
}