/*
Here we configure just the network ACLs, that will be assigned to the subnets
that we have defined in our VPC. They will be more 'general' rules that are
applied at the subnet level if we ever need them.
*/

# The 'public' ACL
resource "aws_network_acl" "public" {
  vpc_id = aws_vpc.main.id
  subnet_ids = [
    aws_subnet.public-0.id,
    aws_subnet.public-1.id
  ]

  ingress {
    protocol   = "-1"
    rule_no    = 1
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = "-1"
    rule_no    = 1
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name        = "Public"
    Environment = var.environment
    Application = var.app_name
  }
}

# The 'private' ACL
resource "aws_network_acl" "private" {
  vpc_id = aws_vpc.main.id
  subnet_ids = [
    aws_subnet.private-0.id,
    aws_subnet.private-1.id,
  ]

  ingress {
    protocol   = "-1"
    rule_no    = 1
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = "-1"
    rule_no    = 1
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name        = "Private"
    Environment = var.environment
    Application = var.app_name
  }
}
