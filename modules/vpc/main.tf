/*
This is where we configure the VPC and all other network components like
internet and nat gateways, route tables, etc. The security part (network ACLs)
is handled in the security.tf file. The security groups - which are also part
of the VPC - will be handeld in the other modules, on a per-module basis
*/

# Define the VPC
resource "aws_vpc" "main" {
  cidr_block            = var.vpc_cidr_block
  enable_dns_support    = true
  enable_dns_hostnames  = true

  tags = {
    Name        = "${var.app_name}-${var.environment}",
    Class       = "main"
    Environment = var.environment
    Application = var.app_name
  }
}

/*
Define an Internet gateway for the VPC so the resources in it can access the
Internet and the Internet can access them back
*/
resource "aws_internet_gateway" "main_igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.app_name}-${var.environment}-igw"
    Environment = var.environment
    Application = var.app_name
  }
}

/*
Define a NAT Gateway to enable resources in the private subnet to connect to
the internet or other AWS services, but prevent the internet from initiating a
connection with those instances
*/
resource "aws_eip" "main_nat_gw" {
  vpc      = true

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}
resource "aws_nat_gateway" "main_nat_gw" {
  allocation_id = aws_eip.main_nat_gw.id
  subnet_id     = aws_subnet.public-1.id

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

/*
The *public* subnets. There are two defined in different availability zones
so redundant services can be deployed
*/
resource "aws_subnet" "public-0" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.vpc_public_cidr_blocks[0]
  availability_zone = var.availability_zones[0]

  tags = {
    Name        = "${var.app_name}-public-0"
    Environment = var.environment
    Application = var.app_name
  }
}
resource "aws_subnet" "public-1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.vpc_public_cidr_blocks[1]
  availability_zone = var.availability_zones[1]

  tags = {
    Name        = "${var.app_name}-public-1"
    Environment = var.environment
    Application = var.app_name
  }
}

/*
The *private* subnets. There are two defined in different availability zones
so redundant services can be deployed
*/
resource "aws_subnet" "private-0" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.vpc_private_cidr_blocks[0]
  availability_zone = var.availability_zones[0]

  tags = {
    Name        = "${var.app_name}-private-0"
    Environment = var.environment
    Application = var.app_name
  }
}
resource "aws_subnet" "private-1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.vpc_private_cidr_blocks[1]
  availability_zone = var.availability_zones[1]

  tags = {
    Name        = "${var.app_name}-private-1"
    Environment = var.environment
    Application = var.app_name
  }
}

# Define the VPC route tables and associate them to the subnets
# The public subnets need to route all traffic to the Internet Gateway
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_igw.id
  }
  route {
    cidr_block         = "10.0.0.0/8"
    gateway_id = aws_internet_gateway.main_igw.id
  }
  tags = {
    Name        = "public-route-table"
    Environment = var.environment
    Application = var.app_name
  }
}
# The private subnets need to route all traffic to the NAT Gateway
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main_nat_gw.id
  }
  route {
    cidr_block         = "10.0.0.0/8"
    gateway_id = aws_internet_gateway.main_igw.id
  }
  tags = {
    Name        = "private-route-table"
    Environment = var.environment
    Application = var.app_name
  }
}
resource "aws_route_table_association" "public-0" {
  subnet_id      = aws_subnet.public-0.id
  route_table_id = aws_route_table.public.id
}
resource "aws_route_table_association" "public-1" {
  subnet_id      = aws_subnet.public-1.id
  route_table_id = aws_route_table.public.id
}
resource "aws_route_table_association" "private-0" {
  subnet_id      = aws_subnet.private-0.id
  route_table_id = aws_route_table.private.id
}
resource "aws_route_table_association" "private-1" {
  subnet_id      = aws_subnet.private-1.id
  route_table_id = aws_route_table.private.id
}
