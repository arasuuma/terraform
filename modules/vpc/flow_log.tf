/*
Here is where we enable the Flow Logs for the VPC
*/

resource "aws_flow_log" "main_flow_log" {
  iam_role_arn    = aws_iam_role.main_vpc_flow_log_role.arn
  log_destination = aws_cloudwatch_log_group.main_vpc_flow_log_group.arn
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.main.id

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

resource "aws_cloudwatch_log_group" "main_vpc_flow_log_group" {
  name = "${var.app_name}-${var.environment}-vpc-flow-log"

  tags = {
    Environment = var.environment
    Application = var.app_name
  }
}

resource "aws_iam_role" "main_vpc_flow_log_role" {
  name = "${var.app_name}_${var.environment}_flow_log_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "main_vpc_flow_log_policy" {
  name = "${var.app_name}_${var.environment}_flow_log_policy"
  role = aws_iam_role.main_vpc_flow_log_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
