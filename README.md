# Terraform

Terraform code for the AWS infrastructure running the application:

Note: I have implemented the IAC code as Terraform for modularized. I don't have personnal aws account  to test and deploy it. I'm sure It will be work.


 1. Implemented terraform code for modularized for all environments.
 2. Each module has  an isolation for the environment.
 3. Need to replace some values in the secific variable like domain name, Account ID,... etc
 4. Automation pipeline for .gitlab-ci.yml file contains the pipeline code. It will deploy the the terraform code to setup the environments in dev,qa and prod.
 5.  I have implemented pipeline has two method.(.gitlab-ci.yml)
     5.1 Method 1: short term credentials are configured in the gitlab configuration. Here am taking the values as parametarized for accesskey and secretkey for each environment file.
	 5.2 method 2:  TF Automation for AWS profile configured and to be  deploye in same way.
 6. Each environments has the branching strategy like dev branch for dev env ,qa branch for qa env  and prod branch for prod env.
 
 
Module list:

1. ACM
2. ALB
3. Cloudwatch
4. Config
5. ECR
6. ECS-Cluster
7. ECS-Service
8. Route53
9. S3
10. SES
11. SNS
12. VPC


Modules are created from environments folder like dev, qa and prod
cd / environments

 Please refer the pipeline file(.gitlab-ci.yml)
 
 
	 