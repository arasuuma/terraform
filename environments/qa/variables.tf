/*
These are global variables that we can use throughout the Terraform code
*/

# The AWS region where we want to bring our infrastructure up
variable "aws_region" {
  type    = string
  default = "eu-central-1"
}

# The availability zones we want to use in the region
variable "aws_availability_zones" {
  type    = list
  default = ["eu-central-1a", "eu-central-1b"]
}

# The environment name in which we are deploying
variable "environment" {
  type    = string
  default = "qa"
}

# The name of the application we are deploying
variable "app_name" {
  type    = string
  default = "bt"
}

variable "endpoint" {
    type = list
    description = "Email endpoints for the subscription users"
    default = ["poovarasu.aws789@gmail.com"]
}

variable "display_name" {

    type = string
    description = "CloudWatch  dashboard dispaly name"
    default = "QA_ENV"

}


/*
Input variables used to configure the 'SES' module
*/
variable "domain_name" {
    type = string
}

variable "email" {
    type = list
    default = ["poovarasu.aws789@gmail.com"]
}


variable "verify_domain" {
  type        = bool
  description = "If provided the module will create Route53 DNS records used for domain verification."
  default     = false
}

variable "verify_dkim" {
  type        = bool
  description = "If provided the module will create Route53 DNS records used for DKIM verification."
  default     = false
}


/*
Input variables used to configure the 'config' module
*/

variable "include_global_resource_types" {

  description = "Specifies whether AWS Config includes all supported types of global resources with the resources that it records."
  type        = bool
  default     = true

}

variable "config_delivery_frequency" {

  description = "The frequency with which AWS Config delivers configuration snapshots."
  default     = "Six_Hours"
  type        = string

}

variable "config_logs_prefix" {

  description = "The S3 prefix for AWS Config logs."
  type        = string
  default     = "Config"

}