/*
This is the 'main' Terraform file for the 'qa' environment. It calls all the
Terraform modules in order to bring up the infrastructure on AWS.
*/

# Configure the VPC, subnets and network resources
module "vpc" {
  source                    = "../../modules/vpc"
  availability_zones        = var.aws_availability_zones
  app_name                  = var.app_name
  environment               = var.environment
  vpc_cidr_block            = "10.62.15.0/24"
  vpc_public_cidr_blocks    = ["10.62.15.0/26", "10.62.15.64/26"]
  vpc_private_cidr_blocks   = ["10.62.15.128/26", "10.62.15.192/26"]
}

# Create a DNS zone file for this environment
module "dns" {
  source      = "../../modules/route53"
  app_name    = var.app_name
  environment = var.environment
  domain_name = "bt.qa.com"  // Replace the Domain name
}

# Create the ECR repositories required to hold the app's Docker images
module "ecr" {
  source        = "../../modules/ecr"
  app_name      = var.app_name
  environment   = var.environment
  repositories  = ["bt-qa"]
}

# Request a Wildcard SSL Certificate for the environment
module "alpha-qa-wildcard" {
  source            = "../../modules/acm"
  app_name          = var.app_name
  environment       = var.environment
  domain_name       = "*.bt.qa.com"   // Replace the Domain name for the application
  alternative_names = ["bt.qa.com"]   // // Replace the Domain name for the application
  dns_zone_id       = module.dns.main_dns_zone_id
}

# Create the S3 buckets required by the application
module "alpha-qa-s3-buckets" {
  source                = "../../modules/s3"
  app_name              = var.app_name
  environment           = var.environment
}

# Create the ECS cluster for this environment
module "alpha-qa-ecs-cluster" {
  source                = "../../modules/ecs-cluster-fargate"
  app_name              = var.app_name
  environment           = var.environment
}

# Deploy the ECS service for the Api application
module "alpha-qa-ecs-service-api" {
  source                = "../../modules/ecs-service-api"
  app_name              = var.app_name
  environment           = var.environment
  vpc_id                = module.vpc.vpc_id
  container_image       = "YYYYYYY.dkr.ecr.eu-central-1.amazonaws.com/bt-qa:latest"   // replace the ecr url
  cpu                   = 1024
  memory                = 2048
  assign_public_ip      = false
  target_type           = "ip"
  health_check_path     = "/"
  health_check_timeout  = 5
  health_check_interval = 30
  health_check_matcher  = "200,401,404"
  private_subnet_ids    = module.vpc.private_subnet_ids
  listen_port           = 8080
  allowed_cidrs         = ["10.62.15.0/24"]
  desired_task_count    = 1
  ecs_cluster_id        = module.alpha-qa-ecs-cluster.cluster_id
  ecs_services_iam_role = module.alpha-qa-ecs-cluster.ecs-service-iam-role-arn
}

# Deploy an Application Load Balancer in front of the ECS services
module "alpha-qa-alb" {
  source                = "../../modules/alb"
  vpc_id                = module.vpc.vpc_id
  app_name              = var.app_name
  environment           = var.environment
  ssl_certificate_arn   = module.alpha-qa-wildcard.certificate_arn
  ssl_policy            = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  public_subnet_ids     = module.vpc.public_subnet_ids
  allowed_cidrs         = ["0.0.0.0/0"]
  internal              = false
  api_tg_arn            = module.alpha-qa-ecs-service-api.ecs_service_tg
  dns_zone_id           = module.dns.main_dns_zone_id
  domain_name           = "bt.qa.com" // Replace the Domain name for the application
}

# Create SNS for the application
module "sns" {

  source                = "../../modules/sns"
  app_name              = var.app_name
  environment           = var.environment
  display_name          = var.display_name
  endpoint              = [var.endpoint]

}  

module "cloudwatch" {

  source                = "../../modules/cloudwatch"
  app_name              = var.app_name
  environment           = var.environment
  api_service_name      = module.alpha-qa-ecs-service-api.api_service_name
  cluster_name          = module.alpha-qa-ecs-cluster.cluster_name
  topic_arn             = module.sns.topic_arn
  alb_name              = module.alpha-qa-alb.alb_name
  api_tg_arn            = module.alpha-qa-ecs-service-api.ecs_service_tg
  
}  

#Create Email subscription for the application
module "ses" {

  source = "../../modules/ses"
  domain_name        = var.domain_name
  main_dns_zone_id   = module.dns.main_dns_zone_id
  verify_dkim   = var.verify_dkim
  verify_domain = var.verify_domain
  email  = var.email

  
}

# Adudit the environment resources in config details
module "config" {

  source = "../../modules/config"
  app_name              = var.app_name
  environment           = var.environment
  config_logs_prefix  = var.config_logs_prefix 
  config_delivery_frequency = var.config_delivery_frequency
  include_global_resource_types = var.include_global_resource_types
}